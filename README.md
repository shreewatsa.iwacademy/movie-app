<<<<<<< README.md
# movie-app


A site that displays movies and also provides search feature.
Build using VueJs and axios to fetch data from http://www.omdbapi.com/ .



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
=======

