import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";

import router from "./router";
import axios from "axios";
// import {store} from './store'

Vue.config.productionTip = false;

axios.defaults.baseURL =
  "http://www.omdbapi.com/?apikey=7497f377&page=1&type=movie&Content-Type=application/json";

new Vue({
  vuetify,
  // store,
  render: h => h(App),
  router
}).$mount("#app");
